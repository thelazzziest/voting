import uuid


def profile_directory_path(instance, filename):
    """Generate path for storing a profile photo."""
    id = instance.user.id if instance.user else uuid.uuid4()
    return 'user_{0}/{1}'.format(id, filename)
