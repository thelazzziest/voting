from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class UsersConfig(AppConfig):
    name = "voting.users"
    verbose_name = _("Users")

    def ready(self):
        try:
            import voting.users.signals  # noqa F401
        except ImportError:
            pass
        try:
            import voting.users.handlers  # noqa F401
        except ImportError:
            pass
