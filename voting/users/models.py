from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import UserManager, PermissionsMixin
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core.mail import send_mail
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel

from voting.core.backends.storages import profile_directory_path


class User(AbstractBaseUser, PermissionsMixin):
    """A fully featured User model with admin-compliant permissions.

    Email and password are required. Other fields are optional."""

    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        validators=(UnicodeUsernameValidator(),),
        help_text=_('150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    email = models.EmailField(
        _('email address'),
        unique=True,
        help_text=_('Required. A valid email'),
        error_messages={
            'unique': _("A user with that email already exists."),
        },
    )
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        indexes = [
            models.Index(fields=('email',)),
        ]

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def get_absolute_url(self):
        return reverse("users:detail", kwargs={"username": self.username})


class Profile(TimeStampedModel):
    """A user profile model."""

    user = models.OneToOneField(User, blank=True, null=True, on_delete=models.CASCADE)

    photo = models.ImageField(upload_to=profile_directory_path, blank=True)
    first_name = models.CharField(_('first name'), max_length=100, blank=True)
    last_name = models.CharField(_('last name'), max_length=150, blank=True)
    patronymic = models.CharField(_('patronymic'), max_length=100, blank=True)

    age = models.PositiveSmallIntegerField(_('age'), blank=True, default=0)
    bio = models.TextField(_('bio'), max_length=500, blank=True)

    @property
    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s %s' % (self.patronymic, self.first_name, self.last_name)
        return full_name.strip()

    @property
    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    class Meta:
        verbose_name = _('profile')
        verbose_name_plural = _('profiles')

