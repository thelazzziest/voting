from typing import Any, Sequence

import names
from django.contrib.auth import get_user_model
from factory import (
    DjangoModelFactory,
    Faker, LazyAttributeSequence,
    post_generation,
    django as django_factory,
)
from factory.fuzzy import FuzzyInteger, FuzzyText

from voting.users.models import Profile


class UserFactory(DjangoModelFactory):
    username = Faker("user_name")
    email = Faker("email")

    @post_generation
    def password(self, create: bool, extracted: Sequence[Any], **kwargs):
        password = Faker(
            "password",
            length=42,
            special_chars=True,
            digits=True,
            upper_case=True,
            lower_case=True,
        ).generate(extra_kwargs={})
        self.set_password(password)

    class Meta:
        model = get_user_model()
        django_get_or_create = ["username"]


class ProfileFactory(DjangoModelFactory):

    photo = django_factory.ImageField()

    first_name = Faker('first_name')
    last_name = Faker('last_name')
    patronymic = LazyAttributeSequence(lambda resolver, index: names.get_last_name())

    age = FuzzyInteger(16, 100)
    bio = FuzzyText(length=100)

    class Meta:
        model = Profile
