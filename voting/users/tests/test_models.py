import pytest

from voting.users.models import User, Profile

pytestmark = pytest.mark.django_db


def test_user_get_absolute_url(user: User):
    assert user.get_absolute_url() == f"/users/{user.username}/"


def test_profile_get_full_name(profile: Profile):
    assert profile.get_full_name == f"{profile.patronymic} {profile.first_name} {profile.last_name}"


def test_profile_get_short_name(profile: Profile):
    assert profile.get_short_name == f"{profile.first_name}"
