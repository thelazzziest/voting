import pytest

from voting.users.forms import UserCreationForm
from voting.users.models import User
from voting.users.tests.factories import UserFactory

pytestmark = pytest.mark.django_db


def test_profile_creation_on_user_creating(user: User):
    user = UserFactory.create()
    assert user.profile is not None
