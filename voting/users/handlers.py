from django.contrib.auth import get_user_model
from django.db.models.base import ModelBase
from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Profile

User = get_user_model()


@receiver(post_save, sender=User)
def create_user_profile(sender: ModelBase, instance: User, created: bool, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender: ModelBase, instance: User, **kwargs):
    instance.profile.save()
