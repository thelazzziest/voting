import pytest
from django.test import RequestFactory

from voting.users.models import User, Profile
from voting.users.tests.factories import UserFactory, ProfileFactory


@pytest.fixture(autouse=True)
def media_storage(settings, tmpdir):
    settings.MEDIA_ROOT = tmpdir.strpath


@pytest.fixture
def user() -> User:
    return UserFactory()


@pytest.fixture
def profile() -> Profile:
    return ProfileFactory()


@pytest.fixture
def request_factory() -> RequestFactory:
    return RequestFactory()
